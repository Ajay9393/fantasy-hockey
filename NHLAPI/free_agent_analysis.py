# -*- coding: utf-8 -*-
"""
Created on Mon Feb  1 13:45:09 2021

@author: Andrew
"""
import requests
import re
import os
import pickle
import pandas as pd
from datetime import datetime, timedelta 
import numpy as np
import matplotlib.pyplot as plt
import sys
import DataAnalysis as aj


def get_added_dropped(list_old, list_new, added_old, dropped_old):
    
    #get new adds and drops each date
    new_adds = [x for x in list_new if x not in list_old]
    new_drops = [x for x in list_old if x not in list_new]
    
    
    #remove new adds/dropped from old list
    temp_adds = list(set(added_old) - set(new_drops))
    temp_drops = list(set(dropped_old) - set(new_adds))
    
    
    #add new drops/adds to old drops/adds
    
    added = temp_adds+new_adds
    dropped = temp_drops+new_drops
    return added,dropped



def return_stats_for_dates(date_start, date_end, player_data, players):
    
 
    num_goalies=0
    temp_goalies = np.zeros((5))
    temp_players = np.zeros((7))
    if players ==[]:
        return temp_players,temp_goalies
    for player in players:
        player = player.replace('.','')

        
        try:
            stats_temp = player_data[player_names[player]]
        except KeyError:
            print ("Player " + player + " has not played game this year")
            continue
        stats_temp.index = pd.to_datetime(stats_temp.index)
        stats = stats_temp.loc[date_old:date_new].copy()
        if stats.empty:
            continue 
        if 'saves' in stats.columns:
            num_goalies+=1
            
            wins = len([x for x in stats['decision'] == 'W' if x])
            SO = len([x for x in stats['savePercentage'] ==100 if x])
            gs = len(stats)
            sv = np.average(stats['savePercentage'])
            times = stats.loc[:,'timeOnIce'].apply(lambda x: x.split(':'))
            times = times.apply(lambda x: int(x[0]) + int(x[1])/60)
            #print (times)
            try:
                ga = np.average((stats.loc[:,'shots'] - stats.loc[:,'saves'])/times*60)
            except ZeroDivisionError:
                ga = 0 
                
            temp_goalies = temp_goalies + np.array([wins, SO, gs,sv,ga])
        
        else:
            stats.loc[:,'STG'] = stats.loc[:,'shortHandedGoals']+stats.loc[:,'powerPlayGoals']
            stats.loc[:,'points'] = stats.loc[:,'goals']+stats.loc[:,'assists']
            temp_players = temp_players + np.sum(stats.loc[:,[
                                                'goals', 'assists','points','plusMinus','faceOffWins',
                                                'hits','STG']])
                
    ##get averages for goalie stats    
    if num_goalies!= 0:
        temp_goalies[2] = temp_goalies[2]/num_goalies
        temp_goalies[3] = temp_goalies[3]/num_goalies
        
    return temp_players, temp_goalies
    


if __name__=="__main__":
    import warnings
    team_change = {'Halifax Highlanders':'SergachevUrSelf B4UWreckUrself'}
    
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=DeprecationWarning)

    num_goalies = 0
    
    with open('Stats/2020_player_names.pickle','rb') as f:
        player_names = pickle.load(f)
    
    with open('Stats/2020_player_data.pickle','rb') as f:
        player_data = pickle.load(f)
        


    files = [x for x in os.listdir() if re.findall('pickle',x) and not re.findall('original',x)]
    
    dates = [datetime.strptime(re.findall(r'\d+-\d+-\d+',file)[0], '%Y-%m-%d') for file in files]
    date_old = r'2021-01-10'
    dates.insert(0,datetime.strptime(date_old,r'%Y-%m-%d' ))
    with open('GDHL_teams_original.pickle','rb') as f:
        initial_rosters = pickle.load(f)
        team_names = list(initial_rosters.keys())
        
    my_dict = {}
        
    for team_name in team_names:
        print (team_name)
        roster_old = initial_rosters[team_name]
        date_old = r'2021-01-10'
        dropped = []
        added = []
        dropped_temp = []
        added_temp = []
        player_data_added = np.zeros((len(files[:])+1, 7))
        goalie_data_added = np.zeros((len(files[:])+1, 5))
        player_data_dropped = np.zeros((len(files[:])+1, 7))
        goalie_data_dropped = np.zeros((len(files[:])+1, 5))
    
        for i,file in enumerate(files):

            date_new = datetime.strptime(re.findall(r'\d+-\d+-\d+',file)[0], '%Y-%m-%d')
            date_new = date_new - timedelta(days = 1)
            date_new = date_new.strftime("%Y-%m-%d")
            
            with open(file,'rb') as f:
                rosters = pickle.load(f)
                try:
                    roster_new =rosters[team_name]
                except KeyError:
                    roster_new = rosters[team_change[team_name]]
                
            added_temp,dropped_temp = get_added_dropped(roster_old, roster_new, added_temp, dropped_temp)
            
            dropped.append(dropped_temp)
            added.append(added_temp)
            
    
            temp_players, temp_goalies = return_stats_for_dates(date_old, date_new, player_data, added_temp)
                
    
            player_data_added[i+1,:] = temp_players
            goalie_data_added[i+1,:] = temp_goalies
            
    
            temp_players,temp_goalies = return_stats_for_dates(date_old, date_new, player_data, dropped_temp)
                
            player_data_dropped[i+1,:] = temp_players
            goalie_data_dropped[i+1,:] = temp_goalies
                
            roster_old = roster_new
            date_old = date_new
            
            my_dict[team_name] = [player_data_added, player_data_dropped, goalie_data_added, goalie_data_dropped, added, dropped]

        
        

for team_name in my_dict.keys():
    player_added_stats = my_dict[team_name][0]
    player_dropped_stats = my_dict[team_name][1]
    goalie_added_stats = my_dict[team_name][2]
    goalie_dropped_stats = my_dict[team_name][3]
    added = my_dict[team_name][4]
    dropped = my_dict[team_name][5]
    added_cumu = np.cumsum(player_added_stats, axis = 0)
    dropped_cumu = np.cumsum(player_dropped_stats, axis = 0)
    goalie_added_cumu  = np.cumsum(goalie_added_stats[:,0:3], axis = 0)
    goalie_dropped_cumu  = np.cumsum(goalie_dropped_stats[:,0:3], axis = 0)
    f, ax = plt.subplots(nrows = 6, ncols = 1, sharex = True, figsize = [15,20])
    categories = ['G','A','P',r'+/-','FOW','hits','STG']
    goal_cat = ['W','SO','GS']
    colors = ['k','r','b']
    for i, cat in enumerate(categories):
        if i<3:
            ax[0].plot(dates[:], added_cumu[:,i] - dropped_cumu[:,i], 'o-',color = colors[i], label = cat)
            
        else:
            ax[i-2].plot(dates[:], added_cumu[:,i] - dropped_cumu[:,i],'o-', color = colors[0], label = cat)
    
    
    for i, cat in enumerate(goal_cat):
        ax[-1].plot(dates[:], goalie_added_cumu[:,i] - goalie_dropped_cumu[:,i], 'o-',label = cat, color  = colors[i])
    
    
    for i,axes in enumerate(ax):
        if i==len(ax)-1:
            aj.figure_quality_axes(axes, 'Date','','',legend = True)
        else:
            aj.figure_quality_axes(axes, '','','',legend = True)
        

    f.tight_layout()
    f.autofmt_xdate()    
    f.suptitle(team_name, fontsize = 24, weight = 'bold',y=1.04)
    f.text(-0.15,0.7, 'Current added list: '+'\n'+'\n'.join(added[-1]), fontsize = 16,verticalalignment = 'top')
    f.text(1.08,0.7, 'Current dropped list: '+ '\n'+'\n'.join(dropped[-1]), fontsize = 16,verticalalignment = 'top')
    
    
    f.savefig('Analysis/'+team_name+ ' player_data.png',format = 'png',bbox_inches='tight')
    plt.close()
    
    

    # f2.tight_layout()
    # f2.autofmt_xdate()    
    # f2.suptitle(team_name, fontsize = 24, weight = 'bold',y=1.04)
    # f2.text(1.08,0.7, 'Current added list: ' +'\n'+'\n'.join(added[-1]))
    # f2.text(1.08,0.4, 'Current dropped list: '+'\n'+'\n'.join(dropped[-1]))
    
    
    # f2.savefig('Analysis/'+team_name+ ' goalie_data.png',format = 'png',bbox_inches='tight')


    