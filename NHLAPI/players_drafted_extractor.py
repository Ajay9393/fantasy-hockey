# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 18:16:42 2021

@author: ak4jo
"""

import openpyxl
import pickle
import csv
import numpy as np
import pandas as pd


columns = ['2016-teams','2016-picks','2017-teams','2017-picks','2018-teams','2018-picks',\
           '2019-teams','2019-picks',\
           '2020-teams','2020-picks',]
test = pd.read_csv('Drafts/draft_history.csv', names = columns)
y1 = test.loc[:,columns[0:2]].copy()
y2 = test.loc[:,columns[2:4]].copy()
y3 = test.loc[:,columns[4:6]].copy()
y4 = test.loc[:,columns[6:8]].copy()
y5 = test.loc[:,columns[8:]].copy()

name_change_maps = {'In Kane we Trust': 'In Marner we Trust',
                    'Juuse Doubles':'JuuseDoubles Mac',
                    "Can't Believe I Lost": 'Flight of the Pigeons',
                    'Moves Like Jagr':'Syracuse Bulldogs',
                    'Michael Cera Fan Club': 'Flight of the Pigeons', ##different manager
                    'Teach Me How to Doughty': 'Halifax Highlanders',
                    'SergachevUrSelf B4UWreckUrself':'Halifax Highlanders'
    
    }

years = []

for draft in [y1, y2, y3, y4,y5]:
    players = {}
    for i, row in draft.iterrows():
        if ~row.isnull().values.any():
            team_name = row.iloc[0]
            player = row.iloc[1]
            if team_name.find('(') != -1:
                team_name = team_name[0:team_name.find('(')].rstrip()
                
            players[player] = (team_name, i)
            
    years.append(players)
    
all_player_data = []
all_player_names = []
for year in ['2016','2017','2018','2019','2020']:
    with open('Stats/{}_player_data.pickle'.format(year),'rb') as f:
        all_player_data.append(pickle.load(f))
    with open('Stats/{}_player_names.pickle'.format(year),'rb') as f:
        all_player_names.append(pickle.load(f))

list_of_years = ['2016','2017','2018','2019','2020']
compiled_df = pd.DataFrame()
for i, draft in enumerate(years):
    for player in draft.keys():
        for k,id_list in enumerate(all_player_names):
            try:
                player_id = id_list[player]
                player_stats = all_player_data[k][player_id]
                player_stats['Player'] = player
                compiled_df = pd.concat([compiled_df,player_stats])
            except KeyError:
                print ("could not find data for {} in year {}".format(player, list_of_years[k]))
                #compiled_df = compiled_df.append(pd.Series(name = player))

compiled_df.reset_index(inplace = True)
compiled_df.set_index('Player',inplace = True)
compiled_df.set_index('index', inplace = True, append = True)
compiled_df.index.rename(['Player', 'Date'], inplace = True)

with open('Drafts/drafted_players_data.pickle','wb') as f:
    pickle.dump(compiled_df,f)
    
with open('Drafts/drafted_players.pickle','wb') as f:
    pickle.dump(years,f)

