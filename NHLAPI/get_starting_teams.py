# -*- coding: utf-8 -*-
"""
Created on Mon Mar 15 00:21:35 2021

@author: ak4jo
"""

import re
from bs4 import BeautifulSoup
import numpy as np
import pandas as pd
import pickle
import datetime

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.webdriver import FirefoxProfile
from selenium import webdriver
import selenium.webdriver.support.ui as ui
now = datetime.datetime.now()
now = datetime.datetime.strftime(now, "%Y-%m-%d")

url = 'https://fantasy.espn.com/hockey/league/draftrecap?leagueId=3399'
driver = webdriver.Firefox()
driver.get(url)

input("Press Enter after logging in")


 
html = driver.page_source
soup = BeautifulSoup(html, 'html.parser')
tables = soup.find_all(class_ = 'InnerLayout__child')
 
 
GDHL_teams = {}
for table in tables:
    players = []
    title =str( table.find_all(class_='Table__Title')[0].span.string)
    print (title)
    body = table.find_all(class_ = 'Table__TBODY')[0]
    for i,child in enumerate(body.children):    
        try:
            player = child.find_all(class_ = 'Table__TD')[1].contents[0].contents[0].contents[0].a.string
            players.append(str(player))
            #print (player)
        except IndexError:
            print ("empty slot!")
    
    GDHL_teams[title] = players
    
with open('GDHL_teams_original.pickle', 'wb') as f:
    pickle.dump(GDHL_teams, f)
    
driver.quit()