# -*- coding: utf-8 -*-
"""
Created on Fri Jul  3 13:15:20 2020

@author: Andrew
"""

##download nhl game data


import requests
import re
import os
import pickle
import pandas as pd
from datetime import datetime
import unidecode
game_data = []
year = '2021'
season_type = '02'
max_game_ID = 900

def multiple_replace(my_dict, text):
  # Create a regular expression  from the dictionary keys
  regex = re.compile("(%s)" % "|".join(map(re.escape, my_dict.keys())))

  # For each match, look-up corresponding value in dictionary
  return regex.sub(lambda mo: my_dict[mo.string[mo.start():mo.end()]], text)


def return_season_data(year, season_type = '02', max_game_ID=1290):

    game_data = []
    time_data = []
    url_template = 'https://statsapi.web.nhl.com/api/v1/game/SEASONTYPEGAME/feed/live'
    for i in range(0, max_game_ID):
        print ("We are at game {}".format(i))
        my_dict = {"SEASON": year, "TYPE": season_type, "GAME": str(i).zfill(4)}
        url = multiple_replace(my_dict,url_template)
        r = requests.get(url = url)
        data = r.json()
        game_data.append(data)
        try:
            date_str = data['gameData']['datetime']['dateTime']
            time_data.append(datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%SZ'))
        except KeyError:
            time_data.append(0)
            print ("Game was not played!")
    with open('Stats/{}.pickle'.format(year), 'wb') as f:
        pickle.dump(game_data, f)
    with open('Stats/{}_games_list.pickle'.format(year), 'wb') as f:
        pickle.dump(time_data, f)

    return game_data, time_data


if __name__ == '__main__':
    seasons = ['2022']

    for season in seasons:
        all_data,games_list = return_season_data(season)
        players_data = {}
        lookup_table = {}

        for game in all_data[1:]:
            try:
                game['messageNumber']
                if game['messageNumber'] == 2:
                    print ("No game found")
            except KeyError:
                print ("Found a game!")
                home_dict = game['liveData']['boxscore']['teams']['home']['players']
                for player_id in home_dict.keys():
                    stats_dict = home_dict[player_id]['stats']
                    if stats_dict == {}:
                        continue
                    player_name = unidecode.unidecode(home_dict[player_id]['person']['fullName']).replace('.','')
                    key = list(stats_dict.keys())[0]
                    date = datetime.strptime(game['gameData']['datetime']['dateTime'], '%Y-%m-%dT%H:%M:%SZ').date()
                    df = pd.DataFrame(stats_dict[key], index = [date])

                    if player_id not in list(players_data.keys()):
                        print ("Adding new player!")
                        lookup_table[player_name] = player_id
                        players_data[player_id] = df


                    else:
                        players_data[player_id] = players_data[player_id].append(df)

                away_dict = game['liveData']['boxscore']['teams']['away']['players']
                for player_id in away_dict.keys():
                    stats_dict = away_dict[player_id]['stats']
                    if stats_dict == {}:
                        continue
                    player_name = away_dict[player_id]['person']['fullName']
                    key = list(stats_dict.keys())[0]
                    date = datetime.strptime(game['gameData']['datetime']['dateTime'], '%Y-%m-%dT%H:%M:%SZ').date()
                    df = pd.DataFrame(stats_dict[key], index = [date])
                    if player_id not in list(players_data.keys()):
                        print ("Adding new player!")
                        lookup_table[player_name] = player_id
                        players_data[player_id] = df

                    else:
                        players_data[player_id] = players_data[player_id].append(df)

        with open('Stats/{}_player_data.pickle'.format(season),'wb') as f:
            pickle.dump(players_data,f)

        with open('Stats/{}_player_names.pickle'.format(season),'wb') as f:
            pickle.dump(lookup_table,f)
