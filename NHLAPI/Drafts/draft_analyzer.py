# -*- coding: utf-8 -*-
"""
Created on Sat Mar 20 16:15:55 2021

@author: Andrew
"""

import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
import re
import sys
sys.path.insert(0,'../')
import DataAnalysis as aj
plot = True

def return_player_dataframe(name):
    temp = players_data.loc[name].copy()
    temp['Player'] = name
    temp.set_index('Player',inplace = True, append = True)
    temp = temp.reorder_levels([1,0])
    
    return temp

with open('drafted_players.pickle','rb') as f:
    players_drafted = pickle.load(f)
    
    
with open('drafted_players_data.pickle','rb') as f:
    players_data = pickle.load(f)
    
name_change_maps = {'In Kane we Trust'.lower(): 'In Marner we Trust'.lower(),
                    'in giroux we trust':'in marner we trust',
                    'Juuse Doubles'.lower():'JuuseDoubles Mac'.lower(),
                    "Can't Believe I Lost".lower(): 'Flight of the Pigeons'.lower(),
                    'Moves Like Jagr'.lower():'Syracuse Bulldogs'.lower(),
                    'Michael Cera Fan Club'.lower(): 'Flight of the Pigeons'.lower(), ##different manager
                    'Teach Me How to Doughty'.lower(): 'Halifax Highlanders'.lower(),
                    'SergachevUrSelf B4UWreckUrself'.lower():'Halifax Highlanders'.lower(),
                    'big pekka':'Halifax Highlanders'.lower(),
                    'sergachevurself':'Halifax Highlanders'.lower(),
    
    }



years = ['2016','2017','2018','2019']

with open('../GDHL_teams_original.pickle','rb') as f:
    teams = pickle.load(f)
    team_names =  list(teams.keys())
    
team_data = {}
list_of_players_not_playing = {}
team_averages = {}
for team in team_names:
    team = team.lower().rstrip()
    team = re.sub(' +', ' ',team)
    team_data[team.lower().rstrip()] = pd.DataFrame()
    list_of_players_not_playing[team.lower().rstrip()] = []
    team_averages[team.lower().rstrip()] = pd.DataFrame()

round_data = {}
list_of_players_not_playing_round={}
for i in range(0,5):
    round_data[i] = pd.DataFrame()
    list_of_players_not_playing_round[i]=[]
    
    
for i, year in enumerate(years):
    for player in players_drafted[i].keys():
        team_name = players_drafted[i][player][0]
        team_name = team_name.lower().rstrip()
        team_name = re.sub(' +', ' ',team_name)
        round_drafted = min(players_drafted[i][player][1]//14,4)
        try:
            team_name = name_change_maps[team_name]
        except KeyError:
            team_name = team_name
        
        try:
            player_data = return_player_dataframe(player)
            player_data['Round'] = round_drafted
            team_data[team_name] = pd.concat([team_data[team_name], player_data])
            round_data[round_drafted] = pd.concat([round_data[round_drafted], player_data])
        except KeyError:
            print ("{} has not played in the NHL!".format(player))
            list_of_players_not_playing[team_name].append(player)
            list_of_players_not_playing_round[round_drafted].append(player)
        



for team_name in team_data.keys():
    temp_df = team_data[team_name].copy()
    
    temp_df['points'] = temp_df['goals']+temp_df['assists']
    temp_df['STG'] = temp_df['powerPlayGoals']+temp_df['shortHandedGoals']
    
    temp_df['wins'] = temp_df['decision'].apply(lambda x: 1 if x=='W' else 0)
    temp_df['SO'] = temp_df['savePercentage'].apply(lambda x: 1 if x==100 else 0)
    temp_df['GS'] = temp_df['decision'].apply(lambda x: 1 if x=='W' or x=='L' else 0)
    team_averages[team_name] = temp_df.copy()
    df_players_to_use = temp_df.loc[:,['goals','assists','points','plusMinus','faceOffWins','hits','STG']]
    df_goalies_to_use = temp_df.loc[:,['wins','SO','GS']]
    
    df_players_to_use = df_players_to_use.reorder_levels([1,0]).sort_index()
    df_goalies_to_use = df_goalies_to_use.reorder_levels([1,0]).sort_index()
    
    

    
    df_players_to_use = df_players_to_use.groupby('Date').sum()
    df_goalies_to_use = df_goalies_to_use.groupby('Date').sum()
    df_players_to_use = df_players_to_use.cumsum()
    df_goalies_to_use = df_goalies_to_use.cumsum()
    
    if plot:
        f, ax = plt.subplots(nrows = 6, ncols = 1, sharex = True, figsize = [15,20])
        categories = ['G','A','P',r'+/-','FOW','hits','STG']
        goal_cat = ['W','SO','GS']
        colors = ['k','r','b']
        dates = df_players_to_use.index.get_level_values('Date')
        for i, cat in enumerate(categories):
            if i<3:
                ax[0].scatter(dates, df_players_to_use.values[:,i], color = colors[i], label = cat)
                
            else:
                ax[i-2].scatter(dates[:], df_players_to_use.values[:,i], color = colors[0], label = cat)
        
        
        for i, cat in enumerate(goal_cat):
            ax[-1].scatter(dates[:], df_goalies_to_use.values[:,i], label = cat, color  = colors[i])
        
        
        for i,axes in enumerate(ax):
            if i==len(ax)-1:
                aj.figure_quality_axes(axes, 'Date','','',legend = True)
            else:
                aj.figure_quality_axes(axes, '','','',legend = True)
        
        f.tight_layout()
        f.autofmt_xdate()    
        f.suptitle(team_name, fontsize = 24, weight = 'bold',y=1.04)
       
        
        
        f.savefig(team_name+ '_cummulative_production.png',format = 'png',bbox_inches='tight')
        plt.close()

goals = []
assists = []
points = []
FOW = []
plusminus = []
hits = []
STG = []
W = []
SO = []
GS = []


for _round in list(round_data.keys()):
    temp_df = round_data[_round].copy()
    temp_df['points'] = temp_df['goals']+temp_df['assists']
    temp_df['STG'] = temp_df['powerPlayGoals']+temp_df['shortHandedGoals']
    
    temp_df['wins'] = temp_df['decision'].apply(lambda x: 1 if x=='W' else 0)
    temp_df['SO'] = temp_df['savePercentage'].apply(lambda x: 1 if x==100 else 0)
    temp_df['GS'] = temp_df['decision'].apply(lambda x: 1 if x=='W' or x=='L' else 0)
    df_players_to_use = temp_df.loc[:,['goals','assists','points','plusMinus','faceOffWins','hits','STG']]
    df_goalies_to_use = temp_df.loc[:,['wins','SO','GS']]
    
    df_players_to_use = df_players_to_use.reorder_levels([1,0]).sort_index()
    df_goalies_to_use = df_goalies_to_use.reorder_levels([1,0]).sort_index()
    df_players_to_use = df_players_to_use.groupby('Date').sum()
    df_goalies_to_use = df_goalies_to_use.groupby('Date').sum()
    df_players_to_use = df_players_to_use.sum()
    df_goalies_to_use = df_goalies_to_use.sum()
    goals.append(df_players_to_use.loc['goals'])
    assists.append(df_players_to_use.loc['assists'])
    points.append(df_players_to_use.loc['points'])
    FOW.append(df_players_to_use.loc['faceOffWins'])
    plusminus.append(df_players_to_use.loc['plusMinus'])
    hits.append(df_players_to_use.loc['hits'])
    STG.append(df_players_to_use.loc['STG'])
    W.append(df_goalies_to_use.loc['wins'])
    SO.append(df_goalies_to_use.loc['SO'])
    GS.append(df_goalies_to_use.loc['GS'])
    
if plot:
    f, ax = plt.subplots(nrows = 6, ncols = 1, sharex = True, figsize = [15,20])
    categories = ['G','A','P',r'+/-','FOW','hits','STG']
    goal_cat = ['W','SO','GS']
    colors = ['k','r','b']
    rounds = ['Round 1','Round 2','Round 3',' Round 4',' Round 5']
    x = np.arange(len(rounds))
    width = 0.35
    ax[0].bar(x-2*width/3, goals,2*width/3, color = colors[0], label = 'G')
    ax[0].bar(x, assists,2*width/3, color = colors[1], label = 'A')
    ax[0].bar(x+2*width/3, points,2*width/3, color = colors[2], label = 'P')
    ax[1].bar(x, FOW, label = 'FOW',width = 2*width/2)
    ax[2].bar(x, hits, label = 'hits',width = 2*width/2)
    ax[3].bar(x, plusminus, label = r'+/-',width = 2*width/2)
    ax[4].bar(x, STG, label = 'STG',width = 2*width/2)
    
    ax[5].bar(x - 2*width/3, W, 2*width/3,label = 'W', color = colors[0])
    ax[5].bar(x, SO, width/3,label = 'SO', color = colors[1])
    ax[5].bar(x+2*width/3, GS,2*width/3, label = 'GS', color = colors[2])
    ax[5].set_xticks(np.arange(len(x)))
    ax[5].set_xticklabels(rounds, rotation = 45)
    
    
    for axes in ax:
        aj.figure_quality_axes(axes, '','','',legend = True)
        
    
    f.tight_layout()
    f.autofmt_xdate()    
    f.suptitle('League Production by Round', fontsize = 24, weight = 'bold',y=1.04)
    f.savefig('League_production_by_round.png',format = 'png')
    plt.close()


##team drafting efficacy
actual_averages = {}
avs_to_plot = {}
std_to_plot = {}
pct_players_in_round = {}
for team in team_averages.keys():
    actual_averages[team] = {}
    pct_players_in_round[team] = []
    avs_to_plot[team] = pd.DataFrame(
        columns = ['goals','assists','points','plusMinus','faceOffWins','hits','STG','wins','SO','GS'],
        index = [0,1,2,3,4]
        )
    std_to_plot[team] = pd.DataFrame(
        columns = ['goals','assists','points','plusMinus','faceOffWins','hits','STG','wins','SO','GS'],
        index = [0,1,2,3,4]
        )
    for _round in list(round_data.keys()):
        performance_for_round = team_averages[team][team_averages[team]['Round']==_round].copy()
        performance_for_round = performance_for_round.groupby('Player').mean()
        players_who_suck = [x for x in list_of_players_not_playing[team] if x in list_of_players_not_playing_round[_round]]
        print (team, _round, len(players_who_suck), len(performance_for_round))
        try:
            pct_players_in_round[team].append(len(performance_for_round)/(len(performance_for_round)+len(players_who_suck)))
        except ZeroDivisionError:
            pct_players_in_round[team].append(0)
            
        for player in players_who_suck:
            performance_for_round = performance_for_round.append(pd.Series(name = player, dtype = float))
        
        performance_for_round.fillna(value = 0., inplace = True)
        print (len(performance_for_round))
        avs = performance_for_round.mean()[['goals','assists','points','plusMinus','faceOffWins','hits','STG','wins','SO','GS']]
        std = performance_for_round.std()[['goals','assists','points','plusMinus','faceOffWins','hits','STG','wins','SO','GS']]
        actual_averages[team][_round] = [avs,std]
        avs_to_plot[team].loc[_round,:] = avs
        std_to_plot[team].loc[_round,:] = std


for team in avs_to_plot.keys():
    f, ax = plt.subplots(nrows = 6, ncols = 1, sharex = True, figsize = [15,20])
    categories = ['G','A','P',r'+/-','FOW','hits','STG']
    goal_cat = ['W','SO','GS']
    colors = ['k','r','b']
    rounds = ['Round 1','Round 2','Round 3',' Round 4',' Round 5']
    width = 0.35
    x = np.arange(len(rounds))
    data = avs_to_plot[team].fillna(0)
    std = std_to_plot[team].fillna(0)
    ax[0].bar(x-2*width/3,data['goals'],yerr = std['goals'], width = 2*width/2, ecolor = 'k',label = 'G', capsize = 5)
    ax[0].bar(x,data['assists'],yerr = std['assists'],width = 2*width/2, label = 'A',ecolor = 'k', capsize = 5)
    ax[0].bar(x+2*width/3,data['points'],yerr = std['points'],width = 2*width/2, label = 'P',ecolor = 'k', capsize = 5)
    ax[1].bar(x,data['faceOffWins'],yerr = std['faceOffWins'],width = 2*width/2, label = 'FOW',ecolor = 'k', capsize = 5)
    ax[2].bar(x,data['plusMinus'],yerr = std['plusMinus'],width = 2*width/2, label = r'+/-',ecolor = 'k', capsize = 5)
    ax[3].bar(x,data['hits'],yerr = std['hits'],width = 2*width/2, label = 'hits',ecolor = 'k', capsize = 5)    
    ax[4].bar(x,data['STG'],yerr = std['STG'],width = 2*width/2, label = 'STG',ecolor = 'k', capsize = 5)
    ax[5].bar(x-2*width/3,data['GS'],yerr = std['GS'],width = 2*width/2, label = 'GS',ecolor = 'k')
    ax[5].bar(x,data['wins'],yerr = std['wins'],width = 2*width/2, label = 'W',ecolor = 'k', capsize = 5)
    ax[5].bar(x+2*width/3,data['SO'],yerr = std['SO'],width = 2*width/2, label = 'SO',ecolor = 'k', capsize = 5)

    for i,pct in enumerate(pct_players_in_round[team]):
        ax[0].text(x[i],ax[0].get_ylim()[1]*1.1, round(pct,2), fontsize = 24, weight = 'bold')
    
    #ax[0].text(-2,ax[0].get_ylim()[1]*1.1, '% Players in NHL', fontsize = 24, weight = 'bold', )
    ax[5].set_xticks(np.arange(len(x)))
    ax[5].set_xticklabels(rounds, rotation = 45)

    for axes in ax:
        aj.figure_quality_axes(axes, '','','',legend = True)
    
    
    temp = team_data[team].copy()
    temp['points'] = temp['goals']+ temp['assists']
    best_players = temp.groupby('Player').sum().sort_values(by = 'points', ascending = False).index
    
    
    f.text(-0.2,0.6, 'Best Performers: '+'\n'+'\n'+'\n'.join(best_players[:5]), fontsize = 20,weight = 'bold',verticalalignment = 'top',horizontalalignment ='left')
    f.text(1.02,0.6, 'Not in NHL: '+ '\n'+'\n'+'\n'.join(list_of_players_not_playing[team]), weight = 'bold',fontsize = 20,verticalalignment = 'top',horizontalalignment ='left')
    f.tight_layout()
    f.suptitle('{}_average_by_round'.format(team), fontsize = 24, weight = 'bold',y=1.04)
    f.savefig('{}_average_by_round.png'.format(team),fmt = 'png',bbox_inches='tight')
    plt.close()

pct_players_in_round_league = []

goals = []
assists = []
points = []
FOW = []
plusminus = []
hits = []
STG = []
W = []
SO = []
GS = []

goals_std = []
assists_std = []
points_std = []
FOW_std = []
plusminus_std = []
hits_std = []
STG_std = []
W_std = []
SO_std = []
GS_std = []

for _round in list(round_data.keys()):
    temp_df = round_data[_round].copy()
    temp_df['points'] = temp_df['goals']+temp_df['assists']
    temp_df['STG'] = temp_df['powerPlayGoals']+temp_df['shortHandedGoals']
    
    temp_df['wins'] = temp_df['decision'].apply(lambda x: 1 if x=='W' else 0)
    temp_df['SO'] = temp_df['savePercentage'].apply(lambda x: 1 if x==100 else 0)
    temp_df['GS'] = temp_df['decision'].apply(lambda x: 1 if x=='W' or x=='L' else 0)
    
    temp_df = temp_df.groupby('Player').mean()
    try:
        pct_players_in_round_league.append(
            len(temp_df)/(len(temp_df)+len(list_of_players_not_playing_round)))
        
    except ZeroDivisionError:
        pct_players_in_round_league.append(0)
        
    for player in list_of_players_not_playing_round:
        temp_df = temp_df.append(pd.Series(name = player, dtype = float))
    
    temp_df = temp_df.fillna(0)
    means = temp_df.mean()
    stds = temp_df.std()
    
    goals.append(means['goals'])
    assists.append(means['assists'])
    points.append(means['points'])
    FOW.append(means['faceOffWins'])
    plusminus.append(means['plusMinus'])
    hits.append(means['hits'])
    STG.append(means['STG'])
    W.append(means['wins'])
    SO.append(means['SO'])
    GS.append(means['GS'])
    
    goals_std.append(stds['goals'])
    assists_std.append(stds['assists'])
    points_std.append(stds['points'])
    FOW_std.append(stds['faceOffWins'])
    plusminus_std.append(stds['plusMinus'])
    hits_std.append(stds['hits'])
    STG_std.append(stds['STG'])
    W_std.append(stds['wins'])
    SO_std.append(stds['SO'])
    GS_std.append(stds['GS'])

    
if plot:
    f, ax = plt.subplots(nrows = 6, ncols = 1, sharex = True, figsize = [15,20])
    categories = ['G','A','P',r'+/-','FOW','hits','STG']
    goal_cat = ['W','SO','GS']
    colors = ['k','r','b']
    rounds = ['Round 1','Round 2','Round 3',' Round 4',' Round 5']
    x = np.arange(len(rounds))
    width = 0.35
    ax[0].bar(x-2*width/3, goals,yerr = goals_std,width = 2*width/3, color = colors[0], label = 'G', capsize = 5)
    ax[0].bar(x, assists,yerr = assists_std,width= 2*width/3, color = colors[1], label = 'A', capsize = 5)
    ax[0].bar(x+2*width/3, points,yerr = points_std,width= 2*width/3, color = colors[2], label = 'P', capsize = 5)
    ax[1].bar(x, FOW, yerr = FOW_std, label = 'FOW',width = 2*width/2, capsize = 5)
    ax[2].bar(x, hits,yerr = hits_std, label = 'hits',width = 2*width/2, capsize = 5)
    ax[3].bar(x, plusminus,yerr=plusminus_std ,label = r'+/-',width = 2*width/2, capsize = 5)
    ax[4].bar(x, STG,yerr=STG_std, label = 'STG',width = 2*width/2, capsize = 5)
    
    ax[5].bar(x - 2*width/3, W,yerr = W_std,width= 2*width/3,label = 'W', color = colors[0], capsize = 5)
    ax[5].bar(x, SO, yerr = SO_std,width= width/3,label = 'SO', color = colors[1], capsize = 5)
    ax[5].bar(x+2*width/3, GS,yerr = GS_std, width =2*width/3, label = 'GS', color = colors[2], capsize = 5)
    ax[5].set_xticks(np.arange(len(x)))
    ax[5].set_xticklabels(rounds, rotation = 45)
    
    for i,pct in enumerate(pct_players_in_round_league):
        ax[0].text(x[i],ax[0].get_ylim()[1]*1.1, round(pct,2), fontsize = 24, weight = 'bold')
        
    for axes in ax:
        aj.figure_quality_axes(axes, '','','',legend = True)
        
    
    f.tight_layout()
    f.autofmt_xdate()    
    f.suptitle('League Averages by Round', fontsize = 24, weight = 'bold',y=1.04)
    f.savefig('League_Averages_by_round.png',format = 'png', bbox_inches= 'tight')
    plt.close()


